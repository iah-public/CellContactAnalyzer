/*-
 * #%L
 * Fiji distribution of ImageJ for the life sciences.
 * %%
 * Copyright (C) 2015 - 2022 Fiji developers.
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.pasteur.iah.ccct;

import static fr.pasteur.iah.ccct.trackmate.CellContactDetectorFactory.KEY_CHANNEL_1;
import static fr.pasteur.iah.ccct.trackmate.CellContactDetectorFactory.KEY_CHANNEL_2;
import static fr.pasteur.iah.ccct.trackmate.CellContactDetectorFactory.KEY_CONTACT_SENSITIVITY;
import static fr.pasteur.iah.ccct.trackmate.CellContactDetectorFactory.KEY_SIGMA_FILTER;
import static fr.pasteur.iah.ccct.trackmate.CellContactDetectorFactory.KEY_THRESHOLD_1;
import static fr.pasteur.iah.ccct.trackmate.CellContactDetectorFactory.KEY_THRESHOLD_2;

import java.util.Map;

import fiji.plugin.trackmate.Model;
import fiji.plugin.trackmate.util.TMUtils;
import fr.pasteur.iah.ccct.trackmate.CellContactConfigurationPanel;
import ij.IJ;
import ij.ImagePlus;
import net.imagej.ImgPlus;
import net.imagej.axis.Axes;
import net.imglib2.RandomAccessibleInterval;
import net.imglib2.img.Img;
import net.imglib2.img.ImgFactory;
import net.imglib2.img.display.imagej.ImageJFunctions;
import net.imglib2.img.planar.PlanarImgFactory;
import net.imglib2.util.Util;
import net.imglib2.view.Views;

class CCCTConfigPanel extends CellContactConfigurationPanel
{

	private final ImagePlus imp;

	public CCCTConfigPanel( final ImagePlus imp, final Model model )
	{
		super( imp, model );
		this.imp = imp;
		this.lblThreshold.setVisible( false );
		this.jtfThreshold.setVisible( false );
		this.lblPixels.setVisible( false );
	}

	@Override
	protected void preview()
	{
		btnPreview.setEnabled( false );
		new Thread( "TrackMate preview detection thread" )
		{
			@SuppressWarnings( { "rawtypes", "unchecked" } )
			@Override
			public void run()
			{
				try
				{
					final Map< String, Object > settings = getSettings();

					// In ImgLib2, dimensions are 0-based.
					final int channel1 = ( Integer ) settings.get( KEY_CHANNEL_1 ) - 1;
					final int channel2 = ( Integer ) settings.get( KEY_CHANNEL_2 ) - 1;
					final int contactSensitivity = ( Integer ) settings.get( KEY_CONTACT_SENSITIVITY );
					final double sigma = ( Double ) settings.get( KEY_SIGMA_FILTER );

					RandomAccessibleInterval im1;
					RandomAccessibleInterval im2;

					final ImgPlus img = TMUtils.rawWraps( imp );
					final int cDim = img.dimensionIndex( Axes.CHANNEL );
					if ( cDim < 0 )
					{
						im1 = img;
						im2 = img;
					}
					else
					{
						im1 = Views.hyperSlice( img, cDim, channel1 );
						im2 = Views.hyperSlice( img, cDim, channel2 );
					}

					int timeDim = img.dimensionIndex( Axes.TIME );
					final int frame = imp.getFrame() - 1;
					if ( timeDim >= 0 )
					{
						if ( cDim >= 0 && timeDim > cDim )
						{
							timeDim--;
						}
						im1 = Views.hyperSlice( im1, timeDim, frame );
						im2 = Views.hyperSlice( im2, timeDim, frame );
					}

					final double threshold_C1 = ( Double ) settings.get( KEY_THRESHOLD_1 );
					final double threshold_C2 = ( Double ) settings.get( KEY_THRESHOLD_2 );

					final ImgFactory factory = new PlanarImgFactory( Util.getTypeFromInterval( im1 ) );
					final Img out = factory.create( im1 );

					final ContactImgGenerator generator = new ContactImgGenerator( im1, im2, out,
							threshold_C1, threshold_C2, contactSensitivity, sigma );

					if ( !generator.checkInput() || !generator.process() )
					{
						IJ.error( CCCT.PLUGIN_NAME + " v" + CCCT.PLUGIN_VERSION, generator.getErrorMessage() );
						return;
					}

					final ImagePlus wrap = ImageJFunctions.wrap( out, CCCT.PLUGIN_NAME + " preview frame " + ( frame + 1 ) );
					wrap.setCalibration( imp.getCalibration() );
					wrap.getProcessor().resetMinAndMax();
					wrap.show();
				}
				finally
				{
					btnPreview.setEnabled( true );
				}

			};
		}.start();
	}

	private static final long serialVersionUID = 1L;

}
