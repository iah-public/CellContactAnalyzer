/*-
 * #%L
 * Fiji distribution of ImageJ for the life sciences.
 * %%
 * Copyright (C) 2015 - 2022 Fiji developers.
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.pasteur.iah.ccct.trackmate;

import static fiji.plugin.trackmate.detection.DetectorKeys.KEY_TARGET_CHANNEL;
import static fiji.plugin.trackmate.detection.DetectorKeys.KEY_THRESHOLD;
import static fiji.plugin.trackmate.gui.Fonts.BIG_FONT;
import static fiji.plugin.trackmate.gui.Fonts.FONT;
import static fr.pasteur.iah.ccct.trackmate.CellContactDetectorFactory.KEY_CHANNEL_1;
import static fr.pasteur.iah.ccct.trackmate.CellContactDetectorFactory.KEY_CHANNEL_2;
import static fr.pasteur.iah.ccct.trackmate.CellContactDetectorFactory.KEY_CONTACT_SENSITIVITY;
import static fr.pasteur.iah.ccct.trackmate.CellContactDetectorFactory.KEY_SIGMA_FILTER;
import static fr.pasteur.iah.ccct.trackmate.CellContactDetectorFactory.KEY_THRESHOLD_1;
import static fr.pasteur.iah.ccct.trackmate.CellContactDetectorFactory.KEY_THRESHOLD_2;

import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JSlider;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import fiji.plugin.trackmate.Logger;
import fiji.plugin.trackmate.Model;
import fiji.plugin.trackmate.Settings;
import fiji.plugin.trackmate.Spot;
import fiji.plugin.trackmate.SpotCollection;
import fiji.plugin.trackmate.TrackMate;
import fiji.plugin.trackmate.gui.components.ConfigurationPanel;
import fiji.plugin.trackmate.util.JLabelLogger;
import fiji.plugin.trackmate.util.TMUtils;
import fr.pasteur.iah.ccct.CCCT;
import fr.pasteur.iah.ccct.ContactImgGenerator;
import ij.ImagePlus;
import ij.gui.NewImage;
import net.imagej.ImgPlus;
import net.imagej.axis.Axes;
import net.imglib2.RandomAccessibleInterval;
import net.imglib2.type.numeric.RealType;
import net.imglib2.view.Views;

public class CellContactConfigurationPanel extends ConfigurationPanel
{
	private static final long serialVersionUID = 1L;

	private final ImagePlus imp;

	private final Model model;

	protected final JButton btnPreview;

	private final JSlider sliderCh1;

	private final JSlider sliderCh2;

	private final JFormattedTextField jtfContactSensitivity;

	private final JFormattedTextField jtfSigma;

	protected final JFormattedTextField jtfThreshold;

	private final Logger localLogger;

	private final JFormattedTextField jtfThresholdC1;

	private final JFormattedTextField jtfThresholdC2;

	private final JLabel jtfCh2;

	private final JLabel jtfCh1;

	protected final JLabel lblThreshold;

	protected final JLabel lblPixels;

	public CellContactConfigurationPanel( final ImagePlus imp, final Model model )
	{
		this.imp = imp == null ? NewImage.createByteImage( "Blank", 50, 50, 3, NewImage.FILL_BLACK ) : imp;
		this.model = model;

		setBorder( BorderFactory.createEmptyBorder( 5, 5, 5, 5 ) );

		@SuppressWarnings( { "rawtypes", "unchecked" } )
		final Map< String, Object > defaultSettings = new CellContactDetectorFactory().getDefaultSettings();
		final GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 11 };
		gridBagLayout.columnWeights = new double[] { 0.0, 0.0, 1.0, 0.0 };
		gridBagLayout.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0 };
		setLayout( gridBagLayout );

		final JLabel lblTitle = new JLabel( "Settings for detector:" );
		lblTitle.setFont( FONT );
		final GridBagConstraints gbcLblTitle = new GridBagConstraints();
		gbcLblTitle.anchor = GridBagConstraints.NORTH;
		gbcLblTitle.fill = GridBagConstraints.HORIZONTAL;
		gbcLblTitle.insets = new Insets( 0, 0, 5, 0 );
		gbcLblTitle.gridwidth = 4;
		gbcLblTitle.gridx = 0;
		gbcLblTitle.gridy = 0;
		add( lblTitle, gbcLblTitle );

		sliderCh2 = new JSlider( 1, imp == null ? 1 : imp.getNChannels() );

		sliderCh2.addChangeListener( new ChangeListener()
		{
			@Override
			public void stateChanged( final ChangeEvent e )
			{
				jtfCh2.setText( "" + sliderCh2.getValue() );
			}
		} );

		final JLabel lblDetectorName = new JLabel( CellContactDetectorFactory.NAME, CCCT.ICON_SMALL, JLabel.CENTER );
		lblDetectorName.setHorizontalAlignment( SwingConstants.LEFT );
		lblDetectorName.setFont( BIG_FONT );
		final GridBagConstraints gbcLblDetectorName = new GridBagConstraints();
		gbcLblDetectorName.anchor = GridBagConstraints.NORTH;
		gbcLblDetectorName.fill = GridBagConstraints.HORIZONTAL;
		gbcLblDetectorName.insets = new Insets( 0, 0, 5, 0 );
		gbcLblDetectorName.gridwidth = 4;
		gbcLblDetectorName.gridx = 0;
		gbcLblDetectorName.gridy = 1;
		add( lblDetectorName, gbcLblDetectorName );

		final JLabel lblInfo = new JLabel();
		lblInfo.setFont( FONT.deriveFont( Font.ITALIC ) );
		lblInfo.setText( CellContactDetectorFactory.DOC );
		final GridBagConstraints gbcLblInfo = new GridBagConstraints();
		gbcLblInfo.fill = GridBagConstraints.BOTH;
		gbcLblInfo.insets = new Insets( 0, 0, 5, 0 );
		gbcLblInfo.gridwidth = 4;
		gbcLblInfo.gridx = 0;
		gbcLblInfo.gridy = 2;
		add( lblInfo, gbcLblInfo );

		final JLabel lblChannel1 = new JLabel( "Channel 1:" );
		lblChannel1.setFont( FONT );
		final GridBagConstraints gbcLblChannel1 = new GridBagConstraints();
		gbcLblChannel1.fill = GridBagConstraints.BOTH;
		gbcLblChannel1.insets = new Insets( 0, 0, 5, 5 );
		gbcLblChannel1.gridx = 0;
		gbcLblChannel1.gridy = 3;
		add( lblChannel1, gbcLblChannel1 );

		sliderCh1 = new JSlider( 1, imp == null ? 1 : imp.getNChannels() );

		sliderCh1.addChangeListener( new ChangeListener()
		{
			@Override
			public void stateChanged( final ChangeEvent e )
			{
				jtfCh1.setText( "" + sliderCh1.getValue() );
			}
		} );
		final GridBagConstraints gbcSliderCh1 = new GridBagConstraints();
		gbcSliderCh1.anchor = GridBagConstraints.NORTH;
		gbcSliderCh1.fill = GridBagConstraints.HORIZONTAL;
		gbcSliderCh1.insets = new Insets( 0, 0, 5, 5 );
		gbcSliderCh1.gridwidth = 2;
		gbcSliderCh1.gridx = 1;
		gbcSliderCh1.gridy = 3;
		add( sliderCh1, gbcSliderCh1 );

		jtfCh1 = new JLabel( "1" );
		jtfCh1.setHorizontalAlignment( SwingConstants.CENTER );
		jtfCh1.setFont( FONT );
		final GridBagConstraints gbcJtfCh1 = new GridBagConstraints();
		gbcJtfCh1.anchor = GridBagConstraints.NORTH;
		gbcJtfCh1.fill = GridBagConstraints.HORIZONTAL;
		gbcJtfCh1.insets = new Insets( 0, 0, 5, 0 );
		gbcJtfCh1.gridx = 3;
		gbcJtfCh1.gridy = 3;
		add( jtfCh1, gbcJtfCh1 );

		final JLabel lblChannel2 = new JLabel( "Channel 2:" );
		lblChannel2.setFont( FONT );
		final GridBagConstraints gbcLblChannel2 = new GridBagConstraints();
		gbcLblChannel2.fill = GridBagConstraints.BOTH;
		gbcLblChannel2.insets = new Insets( 0, 0, 5, 5 );
		gbcLblChannel2.gridx = 0;
		gbcLblChannel2.gridy = 4;
		add( lblChannel2, gbcLblChannel2 );
		final GridBagConstraints gbcSliderCh2 = new GridBagConstraints();
		gbcSliderCh2.anchor = GridBagConstraints.NORTH;
		gbcSliderCh2.fill = GridBagConstraints.HORIZONTAL;
		gbcSliderCh2.insets = new Insets( 0, 0, 5, 5 );
		gbcSliderCh2.gridwidth = 2;
		gbcSliderCh2.gridx = 1;
		gbcSliderCh2.gridy = 4;
		add( sliderCh2, gbcSliderCh2 );

		final JButton btnThresholdC2 = new JButton( "Threshold C2:" );
		btnThresholdC2.setFont( FONT );
		btnThresholdC2.addActionListener( new ActionListener()
		{

			@Override
			public void actionPerformed( final ActionEvent e )
			{
				estimateThreshold( sliderCh2.getValue() - 1, jtfThresholdC2, btnThresholdC2 );
			}
		} );

		final JButton btnThresholdC1 = new JButton( "Threshold C1:" );
		btnThresholdC1.setFont( FONT );
		btnThresholdC1.addActionListener( new ActionListener()
		{
			@Override
			public void actionPerformed( final ActionEvent e )
			{
				estimateThreshold( sliderCh1.getValue() - 1, jtfThresholdC1, btnThresholdC1 );
			}
		} );

		jtfCh2 = new JLabel( "2" );
		jtfCh2.setHorizontalAlignment( SwingConstants.CENTER );
		jtfCh2.setFont( FONT );
		final GridBagConstraints gbcJtfCh2 = new GridBagConstraints();
		gbcJtfCh2.anchor = GridBagConstraints.NORTH;
		gbcJtfCh2.fill = GridBagConstraints.HORIZONTAL;
		gbcJtfCh2.insets = new Insets( 0, 0, 5, 0 );
		gbcJtfCh2.gridx = 3;
		gbcJtfCh2.gridy = 4;
		add( jtfCh2, gbcJtfCh2 );
		final GridBagConstraints gbcBtnThresholdC1 = new GridBagConstraints();
		gbcBtnThresholdC1.anchor = GridBagConstraints.NORTHWEST;
		gbcBtnThresholdC1.insets = new Insets( 0, 0, 5, 5 );
		gbcBtnThresholdC1.gridwidth = 2;
		gbcBtnThresholdC1.gridx = 0;
		gbcBtnThresholdC1.gridy = 5;
		add( btnThresholdC1, gbcBtnThresholdC1 );

		jtfThresholdC1 = new JFormattedTextField( Double.valueOf( 200. ) );
		jtfThresholdC1.setHorizontalAlignment( SwingConstants.CENTER );
		jtfThresholdC1.setFont( FONT );
		final GridBagConstraints gbcJtfThresholdC1 = new GridBagConstraints();
		gbcJtfThresholdC1.fill = GridBagConstraints.HORIZONTAL;
		gbcJtfThresholdC1.insets = new Insets( 0, 0, 5, 5 );
		gbcJtfThresholdC1.gridx = 2;
		gbcJtfThresholdC1.gridy = 5;
		add( jtfThresholdC1, gbcJtfThresholdC1 );
		final GridBagConstraints gbcBtnThresholdC2 = new GridBagConstraints();
		gbcBtnThresholdC2.anchor = GridBagConstraints.NORTHWEST;
		gbcBtnThresholdC2.insets = new Insets( 0, 0, 5, 5 );
		gbcBtnThresholdC2.gridwidth = 2;
		gbcBtnThresholdC2.gridx = 0;
		gbcBtnThresholdC2.gridy = 6;
		add( btnThresholdC2, gbcBtnThresholdC2 );

		jtfThresholdC2 = new JFormattedTextField( Double.valueOf( 200. ) );
		jtfThresholdC2.setHorizontalAlignment( SwingConstants.CENTER );
		jtfThresholdC2.setFont( FONT );
		final GridBagConstraints gbcJtfThresholdC2 = new GridBagConstraints();
		gbcJtfThresholdC2.fill = GridBagConstraints.HORIZONTAL;
		gbcJtfThresholdC2.insets = new Insets( 0, 0, 5, 5 );
		gbcJtfThresholdC2.gridx = 2;
		gbcJtfThresholdC2.gridy = 6;
		add( jtfThresholdC2, gbcJtfThresholdC2 );

		final JLabel lblContactSize = new JLabel( "Contact sensitivity:" );
		lblContactSize.setFont( FONT );
		final GridBagConstraints gbcLblContactSize = new GridBagConstraints();
		gbcLblContactSize.anchor = GridBagConstraints.WEST;
		gbcLblContactSize.insets = new Insets( 0, 0, 5, 5 );
		gbcLblContactSize.gridwidth = 2;
		gbcLblContactSize.gridx = 0;
		gbcLblContactSize.gridy = 7;
		add( lblContactSize, gbcLblContactSize );

		jtfContactSensitivity = new JFormattedTextField( defaultSettings.get( KEY_CONTACT_SENSITIVITY ) );
		jtfContactSensitivity.setHorizontalAlignment( SwingConstants.CENTER );
		jtfContactSensitivity.setFont( FONT );
		final GridBagConstraints gbcJtfContactSensitivity = new GridBagConstraints();
		gbcJtfContactSensitivity.anchor = GridBagConstraints.NORTH;
		gbcJtfContactSensitivity.fill = GridBagConstraints.HORIZONTAL;
		gbcJtfContactSensitivity.insets = new Insets( 0, 0, 5, 5 );
		gbcJtfContactSensitivity.gridx = 2;
		gbcJtfContactSensitivity.gridy = 7;
		add( jtfContactSensitivity, gbcJtfContactSensitivity );

		final JLabel lblPixels_1 = new JLabel( "pixels" );
		lblPixels_1.setFont( FONT );
		final GridBagConstraints gbcLblPixels_1 = new GridBagConstraints();
		gbcLblPixels_1.anchor = GridBagConstraints.WEST;
		gbcLblPixels_1.insets = new Insets( 0, 0, 5, 0 );
		gbcLblPixels_1.gridx = 3;
		gbcLblPixels_1.gridy = 7;
		add( lblPixels_1, gbcLblPixels_1 );

		final JLabel lblFilterSigma = new JLabel( "Filter sigma:" );
		lblFilterSigma.setFont( FONT );
		final GridBagConstraints gbcLblFilterSigma = new GridBagConstraints();
		gbcLblFilterSigma.fill = GridBagConstraints.HORIZONTAL;
		gbcLblFilterSigma.insets = new Insets( 0, 0, 5, 5 );
		gbcLblFilterSigma.gridwidth = 2;
		gbcLblFilterSigma.gridx = 0;
		gbcLblFilterSigma.gridy = 8;
		add( lblFilterSigma, gbcLblFilterSigma );

		jtfSigma = new JFormattedTextField( defaultSettings.get( KEY_SIGMA_FILTER ) );
		jtfSigma.setHorizontalAlignment( SwingConstants.CENTER );
		jtfSigma.setFont( FONT );
		final GridBagConstraints gbcJtfSigma = new GridBagConstraints();
		gbcJtfSigma.anchor = GridBagConstraints.NORTH;
		gbcJtfSigma.fill = GridBagConstraints.HORIZONTAL;
		gbcJtfSigma.insets = new Insets( 0, 0, 5, 5 );
		gbcJtfSigma.gridx = 2;
		gbcJtfSigma.gridy = 8;
		add( jtfSigma, gbcJtfSigma );

		final JLabel lblPixels_2 = new JLabel( "pixels" );
		lblPixels_2.setFont( FONT );
		final GridBagConstraints gbcLblPixels_2 = new GridBagConstraints();
		gbcLblPixels_2.anchor = GridBagConstraints.WEST;
		gbcLblPixels_2.insets = new Insets( 0, 0, 5, 0 );
		gbcLblPixels_2.gridx = 3;
		gbcLblPixels_2.gridy = 8;
		add( lblPixels_2, gbcLblPixels_2 );

		lblThreshold = new JLabel( "Threshold on contact size:" );
		lblThreshold.setFont( FONT );
		final GridBagConstraints gbcLblThreshold = new GridBagConstraints();
		gbcLblThreshold.anchor = GridBagConstraints.WEST;
		gbcLblThreshold.insets = new Insets( 0, 0, 5, 5 );
		gbcLblThreshold.gridwidth = 2;
		gbcLblThreshold.gridx = 0;
		gbcLblThreshold.gridy = 9;
		add( lblThreshold, gbcLblThreshold );

		jtfThreshold = new JFormattedTextField( defaultSettings.get( KEY_THRESHOLD ) );
		jtfThreshold.setHorizontalAlignment( SwingConstants.CENTER );
		jtfThreshold.setFont( FONT );
		final GridBagConstraints gbcJtfThreshold = new GridBagConstraints();
		gbcJtfThreshold.anchor = GridBagConstraints.NORTH;
		gbcJtfThreshold.fill = GridBagConstraints.HORIZONTAL;
		gbcJtfThreshold.insets = new Insets( 0, 0, 5, 5 );
		gbcJtfThreshold.gridx = 2;
		gbcJtfThreshold.gridy = 9;
		add( jtfThreshold, gbcJtfThreshold );

		lblPixels = new JLabel( "pixels" );
		lblPixels.setFont( FONT );
		final GridBagConstraints gbcLblPixels = new GridBagConstraints();
		gbcLblPixels.anchor = GridBagConstraints.WEST;
		gbcLblPixels.insets = new Insets( 0, 0, 5, 0 );
		gbcLblPixels.gridx = 3;
		gbcLblPixels.gridy = 9;
		add( lblPixels, gbcLblPixels );

		btnPreview = new JButton( "Preview" );
		btnPreview.setFont( FONT );
		btnPreview.addActionListener( new ActionListener()
		{
			@Override
			public void actionPerformed( final ActionEvent arg0 )
			{
				preview();
			}
		} );
		final GridBagConstraints gbcBtnPreview = new GridBagConstraints();
		gbcBtnPreview.gridwidth = 2;
		gbcBtnPreview.insets = new Insets( 10, 0, 5, 0 );
		gbcBtnPreview.anchor = GridBagConstraints.SOUTHEAST;
		gbcBtnPreview.gridx = 2;
		gbcBtnPreview.gridy = 10;
		add( btnPreview, gbcBtnPreview );

		final JLabelLogger labelLogger = new JLabelLogger();
		localLogger = labelLogger.getLogger();
		final GridBagConstraints gbcLabelLogger = new GridBagConstraints();
		gbcLabelLogger.anchor = GridBagConstraints.NORTH;
		gbcLabelLogger.fill = GridBagConstraints.HORIZONTAL;
		gbcLabelLogger.gridwidth = 5;
		gbcLabelLogger.gridx = 0;
		gbcLabelLogger.gridy = 11;
		add( labelLogger, gbcLabelLogger );
	}

	private void estimateThreshold( final int channel, final JFormattedTextField target, final JButton source )
	{
		source.setEnabled( false );
		new Thread( "CCCT Threshold estimator thread" )
		{
			@SuppressWarnings( { "rawtypes", "unchecked" } )
			@Override
			public void run()
			{
				try
				{
					final int frame = imp.getFrame() - 1;
					final ImgPlus img = TMUtils.rawWraps( imp );
					RandomAccessibleInterval slice;

					final int cDim = img.dimensionIndex( Axes.CHANNEL );
					if ( cDim < 0 )
					{
						slice = img;
					}
					else
					{
						slice = Views.hyperSlice( img, cDim, channel );
					}

					int timeDim = img.dimensionIndex( Axes.TIME );
					if ( timeDim >= 0 )
					{
						if ( cDim >= 0 && timeDim > cDim )
						{
							timeDim--;
						}
						slice = Views.hyperSlice( slice, timeDim, frame );
					}
					final RealType threshold = ContactImgGenerator.otsuTreshold( slice );
					target.setValue( Double.valueOf( threshold.getRealDouble() ) );
				}
				finally
				{
					source.setEnabled( true );
				}
			};
		}.start();
	}

	@Override
	public void setSettings( final Map< String, Object > settings )
	{
		sliderCh1.setValue( Math.min( imp.getNChannels(), ( Integer ) settings.get( KEY_CHANNEL_1 ) ) );
		sliderCh2.setValue( Math.min( imp.getNChannels(), ( Integer ) settings.get( KEY_CHANNEL_2 ) ) );
		jtfCh1.setText( "" + sliderCh1.getValue() );
		jtfCh2.setText( "" + sliderCh2.getValue() );

		jtfContactSensitivity.setValue( settings.get( KEY_CONTACT_SENSITIVITY ) );
		jtfSigma.setValue( settings.get( KEY_SIGMA_FILTER ) );
		jtfThreshold.setValue( settings.get( KEY_THRESHOLD ) );
	}

	@Override
	public Map< String, Object > getSettings()
	{
		final HashMap< String, Object > settings = new HashMap< String, Object >( 5 );
		final int channel1 = sliderCh1.getValue();
		final int channel2 = sliderCh2.getValue();
		try
		{
			jtfContactSensitivity.commitEdit();
			jtfSigma.commitEdit();
			jtfThreshold.commitEdit();
			jtfThresholdC1.commitEdit();
			jtfThresholdC2.commitEdit();
		}
		catch ( final ParseException e )
		{
			e.printStackTrace();
		}

		final int contactSensitivity = ( Integer ) jtfContactSensitivity.getValue();
		final double sigma = ( Double ) jtfSigma.getValue();
		final double threshold_C1 = ( Double ) jtfThresholdC1.getValue();
		final double threshold_C2 = ( Double ) jtfThresholdC2.getValue();
		final double threshold = ( Double ) jtfThreshold.getValue();

		settings.put( KEY_CHANNEL_1, channel1 );
		settings.put( KEY_CHANNEL_2, channel2 );
		settings.put( KEY_CONTACT_SENSITIVITY, contactSensitivity );
		settings.put( KEY_SIGMA_FILTER, sigma );
		settings.put( KEY_THRESHOLD, threshold );
		settings.put( KEY_THRESHOLD_1, threshold_C1 );
		settings.put( KEY_THRESHOLD_2, threshold_C2 );
		settings.put( KEY_THRESHOLD, threshold );
		// Add a dummy target channel
		settings.put( KEY_TARGET_CHANNEL, 1 );
		return settings;
	}

	protected void preview()
	{
		btnPreview.setEnabled( false );
		new Thread( "TrackMate preview detection thread" )
		{
			@SuppressWarnings( "rawtypes" )
			@Override
			public void run()
			{
				final Settings settings = new Settings( imp );
				final int frame = imp.getFrame() - 1;
				settings.tstart = frame;
				settings.tend = frame;

				settings.detectorFactory = new CellContactDetectorFactory();
				settings.detectorSettings = getSettings();

				final TrackMate trackmate = new TrackMate( settings );
				trackmate.getModel().setLogger( localLogger );

				final boolean detectionOk = trackmate.execDetection();
				if ( !detectionOk )
				{
					localLogger.error( trackmate.getErrorMessage() );
					return;
				}
				localLogger.log( "Found " + trackmate.getModel().getSpots().getNSpots( false ) + " spots." );

				// Wrap new spots in a list.
				final SpotCollection newspots = trackmate.getModel().getSpots();
				final Iterator< Spot > it = newspots.iterator( frame, false );
				final ArrayList< Spot > spotsToCopy = new ArrayList< Spot >( newspots.getNSpots( frame, false ) );
				while ( it.hasNext() )
				{
					spotsToCopy.add( it.next() );
				}
				// Pass new spot list to model.
				model.getSpots().put( frame, spotsToCopy );
				// Make them visible
				for ( final Spot spot : spotsToCopy )
				{
					spot.putFeature( SpotCollection.VISIBILITY, SpotCollection.ONE );
				}
				// Generate event for listener to reflect changes.
				model.setSpots( model.getSpots(), true );

				btnPreview.setEnabled( true );

			};
		}.start();
	}

	@Override
	public void clean()
	{}
}
