/*-
 * #%L
 * Fiji distribution of ImageJ for the life sciences.
 * %%
 * Copyright (C) 2015 - 2022 Fiji developers.
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.pasteur.iah.ccct;

import ij.CompositeImage;
import ij.IJ;
import ij.ImageJ;
import ij.ImagePlus;
import ij.plugin.ChannelSplitter;
import net.imglib2.img.Img;
import net.imglib2.img.display.imagej.ImageJFunctions;
import net.imglib2.img.planar.PlanarImgFactory;
import net.imglib2.type.NativeType;
import net.imglib2.type.numeric.RealType;
import net.imglib2.view.Views;

public class ContactImgGeneratorTestDrive
{

	public static < T extends RealType< T > & NativeType< T > > void main( final String[] args )
	{
		ImageJ.main( args );

		ImageJ.main( args );
		final String file = "samples/BcellsTcells-small.tif";
		final CompositeImage imp = new CompositeImage( new ImagePlus( file ) );
		imp.setOpenAsHyperStack( true );
		imp.setMode( CompositeImage.COMPOSITE );
		imp.show();

		// Extract individual channels.
		final int c1 = 1; // 0-based
		final int c2 = 2;

		final ImagePlus[] channels = ChannelSplitter.split( imp );
		final ImagePlus ch1 = channels[ c1 ];
		final ImagePlus ch2 = channels[ c2 ];
		final Img< T > img1 = ImageJFunctions.wrap( ch1 );
		final Img< T > img2 = ImageJFunctions.wrap( ch2 );

		// Parameter for the contact detection.
		final int contactSize = 3;
		final double thresholdC1 = 170.;
		final double thresholdC2 = 170.;
		final double sigma = 1.;

		final PlanarImgFactory< T > factory = new PlanarImgFactory< T >( img1.firstElement() );
		final Img< T > out = factory.create( img1 );

		final int d = 2; // Time dimension.
		final int nFrames = ch1.getNFrames();
		long dt = 0;
		System.out.println( "Detecting contacts..." );
		for ( int t = 0; t < nFrames; t++ )
		{
			final ContactImgGenerator< T > algo = new ContactImgGenerator< T >(
					Views.hyperSlice( img1, d, t ),
					Views.hyperSlice( img2, d, t ),
					Views.hyperSlice( out, d, t ),
					thresholdC1,
					thresholdC2,
					contactSize,
					sigma );

			if ( !algo.checkInput() || !algo.process() )
			{
				System.err.println( algo.getErrorMessage() );
				return;
			}

			dt += algo.getProcessingTime();

			if ( t > 50 )
				break;
		}
		System.out.println( "Completed in " + dt / 1e3 + " s." );

		final ImagePlus outImp = ImageJFunctions.wrap( out, "Contacts" );
		outImp.setDimensions( 1, 1, nFrames );
		outImp.setCalibration( imp.getCalibration().copy() );
		outImp.show();
		IJ.run( "Fire" );
		IJ.resetMinAndMax( outImp );
	}
}
